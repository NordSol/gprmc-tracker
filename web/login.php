<?php
include_once "../common/params.php";
include_once "../common/base.php";
include_once "../common/db.php";
include_once "../common/User.php";

$pageTitle = "Authorization";

if (!empty($_POST)) {
    $user = new User();
    $user->login($_POST['username'], $_POST['password']);
}
?>
<?php include_once "../layouts/header.php"; ?>

    <div id="main">

        <?php if (!empty($_SESSION['loggedIn']) && !empty($_SESSION['username'])) : ?>
            <a href="index.php">Proceed to Map</a>
        <?php else : ?>
            <form method="post" action="login.php" name="loginform" id="loginform">
                <div>
                    <input type="text" name="username" id="username" />
                    <label for="username">Username</label>
                    <br /><br />
                    <input type="password" name="password" id="password" />
                    <label for="password">Password</label>
                    <br /><br />
                    <input type="submit" name="login" id="login" value="Login" class="button" />
                </div>
            </form>
        <?php endif; ?>

    </div>

<?php include_once "../layouts/footer.php"; ?>