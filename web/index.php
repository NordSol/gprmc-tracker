<?php
include_once "../common/params.php";
include_once "../common/base.php";
include_once "../common/db.php";
require_once '../common/DevicePosition.php';

if (empty($_SESSION['loggedIn']) && empty($_SESSION['username'])) {
    header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/login.php");
}

$devicePosition = new DevicePosition();
$pageTitle = "GPS info";
?>
<?php include_once "../layouts/header.php"; ?>

<style>
    html, body, #map {
        width: 100%; height: 500px; padding: 0; margin: 0;
    }
</style>

    <div id="main">
        <?php foreach ($devicePosition->getDevicesList() as $device) : ?>
            <?php if (!empty($device)) : ?>
                <a href="index.php?device_id=<?= $device['device_id'] ?>"><?= $device['device_id'] ?></a>
            <?php endif; ?>
        <?php endforeach; ?>

        <div id="map"></div>

    </div>


<script>
  ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
      center: [55.751574, 37.573856],
      zoom: 9
    }, {
      searchControlProvider: 'yandex#search'
    });

      <?php foreach ($devicePosition->getLatestPositions() as $curPos) : ?>
        <?php if (!empty($curPos['longitude'])) :?>
            myMap.geoObjects.add(
              new ymaps.Placemark(
                [<?= $curPos['longitude'] ?>, <?= $curPos['latitude'] ?>], {
                  hintContent: '...',
                  balloonContent: '<?= $curPos['device_id'] ?>',
                  iconContent: '<?= !empty($curPos['device_name']) ? $curPos['device_name'] : $curPos['device_id'] ?>'
                }, {
                  preset: 'islands#blackStretchyIcon',
                }
              )
            );
        <?php endif; ?>
      <?php endforeach; ?>

    myMap.controls.add('typeSelector');


    myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 10) myMap.setZoom(10);});

  });
</script>

<?php include_once "../layouts/footer.php"; ?>