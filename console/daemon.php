#!/usr/bin/php
<?php
require_once '../common/db.php';
require_once '../common/DevicePosition.php';

if (count($argv) == 1) {
    echo "Usage: $argv[0] <start> <stop> <restart>\n";
    exit();
}

switch ($argv[1]) {
    case 'start':
        $__server_listening = true;
        startDaemon($db);
        break;
    case 'stop':
        stopDaemon();
        break;
    case 'restart':
        stopDaemon();
        startDaemon($db);
        break;
}

function stopDaemon() {
    list($scriptPath) = get_included_files();
    $pathInfo = pathinfo($scriptPath);

    $pid = file_get_contents($pathInfo['dirname'].'/daemon.pid');
    echo "Killing: $pid\n";
    exec("kill -9 $pid");
}

function startDaemon($db) {
    /**
     * Listens for requests and forks on each connection
     */

    error_reporting(E_ALL);
    set_time_limit(0);
    ob_implicit_flush();
    declare(ticks=1);

    $pid = become_daemon();
    list($scriptPath) = get_included_files();
    $pathInfo = pathinfo($scriptPath);
    file_put_contents($pathInfo['dirname'].'/daemon.pid', $pid);
    echo "PID: {$pid}\n";

    /* nobody/nogroup, change to your host's uid/gid of the non-priv user */
    change_identity(1000, 1000);

    /* handle signals */
    pcntl_signal(SIGTERM, 'sig_handler');
    pcntl_signal(SIGINT, 'sig_handler');
    pcntl_signal(SIGCHLD, 'sig_handler');

    /* change this to your own host / port */
    server_loop("0.0.0.0", 1234, $db);
}

/**
  * Change the identity to a non-priv user
  */
function change_identity( $uid, $gid )
{
    if( !posix_setgid( $gid ) )
    {
        print "Unable to setgid to " . $gid . "!\n";
        exit;
    }

    if( !posix_setuid( $uid ) )
    {
        print "Unable to setuid to " . $uid . "!\n";
        exit;
    }
}

/**
  * Creates a server socket and listens for incoming client connections
  * @param string $address The address to listen on
  * @param int $port The port to listen on
  */
function server_loop($address, $port, $db)
{
    GLOBAL $__server_listening;

    if(($sock = socket_create(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        echo "failed to create socket: ".socket_strerror($sock)."\n";
        exit();
    }

    if(($ret = socket_bind($sock, $address, $port)) < 0)
    {
        echo "failed to bind socket: ".socket_strerror($ret)."\n";
        exit();
    }

    if( ( $ret = socket_listen( $sock, 0 ) ) < 0 )
    {
        echo "failed to listen to socket: ".socket_strerror($ret)."\n";
        exit();
    }

    socket_set_nonblock($sock);

    echo "waiting for clients to connect\n";

    while ($__server_listening)
    {
        $connection = @socket_accept($sock);
        if ($connection === false) {
            usleep(100);
        } elseif ($connection > 0) {
            handle_client($sock, $connection, $db);
        } else {
            echo "error: ".socket_strerror($connection);
            die;
        }
    }
}

/**
  * Signal handler
  */
function sig_handler($sig)
{
    switch($sig)
    {
        case SIGTERM:
        case SIGINT:
            exit();
        break;

        case SIGCHLD:
            pcntl_waitpid(-1, $status);
        break;
    }
}

/**
  * Handle a new client connection
  */
function handle_client($ssock, $csock, $db)
{
    GLOBAL $__server_listening;

    $pid = pcntl_fork();

    if ($pid == -1) {
        /* fork failed */
        echo "fork failure!\n";
        die;
    } elseif ($pid == 0) {
        /* child process */
        $__server_listening = false;
        socket_close($ssock);
        interact($csock, $db);
        socket_close($csock);
    } else {
        socket_close($csock);
    }
}

function convertRMC2LatLong($data) {
    // Широта:
    $intDeg = (int) ($data[3]/100);
    $lat = $intDeg + ($data[3] - 100 * $intDeg)/60;
    if ($data[4] == 'S') {
        $lat = $lat * (-1);
    }

    // Долгота:
    $intDeg = (int) ($data[5]/100);
    $long = $intDeg + ($data[5] - 100 * $intDeg)/60;
    if ($data[6] == 'W') {
        $long = $lat * (-1);
    }

    /*switch ($data[0]) {
        case 'GPRMC':
            // GPS Format

            break;
        case 'GLRMC':
            // Glonass Format

            break;
        case 'GARMC':
            // Galileo Format

            break;
        case 'GNRMC':
            // Glonass + GPS Format

            break;
    }*/

    return [
        'longitude' => $long, //Долгота
        'latitude' => $lat, //Широта
    ];
}

function interact($socket, $db)
{
    $clientIP = '';
    $clientPort = 0;
    socket_recvfrom($socket, $buffer, 1024, 0, $clientIP, $clientPort);
    list($auth, $gprmc) = explode('$', $buffer);

    $authData = explode('#', $auth);
    $gprmcData = explode(',', $gprmc);

    $coords = convertRMC2LatLong($gprmcData);


    // TODO: Switch to Class methods
    $dsn = "mysql:host=". DB_HOST .";dbname=". DB_NAME;
    $db = new PDO($dsn, DB_USER, DB_PASS);

    $sql = "INSERT INTO device_position(device_id, device_name, longitude, latitude, params)
                VALUES(:deviceId, :deviceName, :long, :lat, :params)";

    if($stmt = $db->prepare($sql)) {
        $params = json_encode(['auth_data' => $authData, 'gps_data' => $gprmcData]);
        $stmt->bindParam(":deviceId", $authData[1], PDO::PARAM_STR);
        $stmt->bindParam(":deviceName", $authData[4], PDO::PARAM_STR);
        $stmt->bindParam(":long", strval($coords['longitude']), PDO::PARAM_STR);
        $stmt->bindParam(":lat", strval($coords['latitude']), PDO::PARAM_STR);
        $stmt->bindParam(":params", $params, PDO::PARAM_STR);

        if (!$stmt->execute()) {
            // TODO: Log error
            file_put_contents('/tmp/errors.log', print_r($stmt->errorInfo(), 1));
        }
    }
}

/**
  * Become a daemon by forking and closing the parent
  */
function become_daemon()
{
    $pid = pcntl_fork();

    if ($pid == -1) {
        /* fork failed */
        echo "fork failure!\n";
        exit();
    } elseif ($pid) {
        /* close the parent */
        exit();
    } else {
        /* child becomes our daemon */
        posix_setsid();
        chdir('/');
        umask(0);
        return posix_getpid();

    }
}

?>