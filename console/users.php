#!/usr/bin/php
<?php
require_once '../common/db.php';
require_once '../common/User.php';

    if (count($argv) == 1) {
        echo "Usage: $argv[0] <action> <param1> <param2> ...\n";
        exit();
    }

    switch ($argv[1]) {
        case 'create':
            if (count($argv) == 2) {
                echo "Usage: $argv[0] create <username> <password> <email> <name>\n";
                break;
            }

            $username = trim($argv[2]);
            $password = isset($argv[3]) ? $argv[3] : null;
            $email = isset($argv[4]) ? $argv[4] : null;
            $name = isset($argv[5]) ? $argv[5] : null;

            $user = new User($db, true);
            $result = $user->create($username, $password, $email, $name);
            if ($result['success']) {
                echo $result['messages'][0]."\n";
                exit(1);
            } else {
                echo $result['messages'][0]."\n";
                exit(0);
            }
            break;
        case 'update':

            break;
        case 'delete':
            if (count($argv) == 2) {
                echo "Usage: $argv[0] delete <username>\n";
                break;
            }

            $username = trim($argv[2]);

            $user = new User($db, true);
            $result = $user->delete($username);
            if ($result['success']) {
                echo $result['messages'][0]."\n";
                exit(1);
            } else {
                echo $result['messages'][0]."\n";
                exit(0);
            }
            break;
        case 'info':

            break;
    }


?>