<?php
    // Include site constants
    include_once "config/db.php";
    include_once "config/app.php";

    // Create a database object
    try {
        $dsn = "mysql:host=". DB_HOST .";dbname=". DB_NAME;
        $db = new PDO($dsn, DB_USER, DB_PASS);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
        exit;
    }
?>