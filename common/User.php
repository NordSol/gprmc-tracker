<?php
class User {
    private $_db;

    private $isAdmin;

    public function __construct($db = null, $isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
        if (is_object($db))
        {
            $this->_db = $db;
        }
        else
        {
            $dsn = "mysql:host=". DB_HOST .";dbname=". DB_NAME;
            $this->_db = new PDO($dsn, DB_USER, DB_PASS);
        }
    }

    public function delete($username)
    {
        if (!$this->isAdmin &&
            (
                empty($_SESSION['loggedIn']) ||
                empty($_SESSION['username']) ||
                empty($_SESSION['userId']) ||
                $username !== $_SESSION['username']
            )
        ) {
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];
        }

        $sql = "SELECT COUNT(username) AS cnt
                FROM user
                WHERE username = :username";

        if($stmt = $this->_db->prepare($sql)) {
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch();

            if($row['cnt'] == 0) {
                return [
                    'success' => false,
                    'messages' => [
                        'Account doesn\'t exist!..',
                    ],
                ];
            }

            $stmt->closeCursor();
        }

        $sql = "DELETE
                FROM user
                WHERE username = :username";
        try
        {
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();

            return [
                'success' => true,
                'messages' => [
                    'Account successfully removed!..'
                ],
            ];
        }
        catch(PDOException $e)
        {
            return [
                'success' => false,
                'messages' => [
                    $e->getMessage()
                ],
            ];
        }
    }

    public function info() {
        if (!$this->isAdmin && (empty($_SESSION['loggedIn']) || empty($_SESSION['username']) || empty($_SESSION['userId'])))
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];

        $sql = "SELECT id, name, email
                FROM user
                WHERE id = :id";
        try
        {
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(':id', $_SESSION['userId'], PDO::PARAM_INT);
            $stmt->execute();
            $user = $stmt->fetch();
            $stmt->closeCursor();

            return [
                'success' => true,
                'id' => $user['id'],
                'name' => $user['name'],
                'email' => $user['email'],
                'messages' => [
                ],
            ];
        }
        catch(PDOException $e)
        {
            return [
                'success' => false,
                'messages' => [
                    $e->getMessage(),
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unknown error...'
            ],
        ];
    }

    public static function logout() {
        if (!empty($_SESSION['loggedIn'])) {
            unset($_SESSION['loggedIn']);
            unset($_SESSION['username']);
            unset($_SESSION['userId']);

            return [
                'success' => true,
                'messages' => [
                    'Successfully logged out...'
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'You are not logged in yet...'
            ],
        ];
    }

    public function login($username, $password) {
        $password = md5($password .':'. AUTH_SALT);

        $sql = "SELECT id
            FROM user
            WHERE username = :username
            AND password = :password
            LIMIT 1";

        try
        {
            $stmt = $this->_db->prepare($sql);
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                $user = $stmt->fetch(PDO::FETCH_ASSOC);

                $_SESSION['username'] = htmlentities($username, ENT_QUOTES);
                $_SESSION['userId'] = $user['id'];
                $_SESSION['loggedIn'] = true;
                return [
                    'success' => true,
                    'messages' => [
                    ],
                ];
            }

            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];
        }
        catch(PDOException $e)
        {
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                    $e->getMessage()
                ],
            ];
        }
    }

    public function update($username, $password = null, $email = null, $name = null) {
        if (!$this->isAdmin && (empty($_SESSION['loggedIn']) || empty($_SESSION['username']) || $_SESSION['username'] !== $username))
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];


        if (!$password && !$email && !$name) return [
            'success' => false,
            'messages' => [
                'Nothing to update...',
            ],
        ];

        $username = trim($username);
        $email = $email ? trim($email) : null;
        $name = $name ? trim($name) : null;

        $sql = "SELECT *
                FROM user
                WHERE username = :username
                LIMIT 1";

        if($stmt = $this->_db->prepare($sql)) {
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                $user = $stmt->fetch(PDO::FETCH_ASSOC); // TODO: check what fields were changed
            } else {
                return [
                    'success' => false,
                    'messages' => [
                        'Username not found!..',
                    ],
                ];
            }

            $stmt->closeCursor();

            $sql = "UPDATE user SET ";
            if ($password !== null) $sql .= "password = :password ";
            if ($email !== null) $sql .= "email = :email ";
            if ($name !== null) $sql .= "name = :name";
            $sql .= " WHERE id = :id";


            if($stmt = $this->_db->prepare($sql)) {
                $stmt->bindParam(":id", $user['id'], PDO::PARAM_INT);
                if ($password !== null) $stmt->bindParam(":password", $password, PDO::PARAM_STR);
                if ($email !== null) $stmt->bindParam(":email", $email, PDO::PARAM_STR);
                if ($name !== null) $stmt->bindParam(":name", $name, PDO::PARAM_STR);
                if (!$stmt->execute()) return [
                    'success' => false,
                    'messages' => [
                        'Unable to update...',
                    ],
                ];

            }

            $stmt->closeCursor();
            return [
                'success' => true,
                'id' => $user['id'],
                'messages' => [
                    'User successfully updated!',
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unspecified error...',
            ],
        ];
    }

    public function create($username, $password, $email = null, $name = null) {
        $username = trim($username);
        $password = md5($password .':'. AUTH_SALT);
        $email = $email ? trim($email) : null;
        $name = $name ? trim($name) : null;

        // Checking if user already exists
        $sql = "SELECT COUNT(username) AS cnt
                FROM user
                WHERE username = :username";

        if($stmt = $this->_db->prepare($sql)) {
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch();

            if($row['cnt'] != 0) {
                return [
                    'success' => false,
                    'messages' => [
                        'Username already registered!..',
                    ],
                ];
            }

            $stmt->closeCursor();
        }

        // Trying to create new user
        $sql = "INSERT INTO user(username, password, email, name)
                VALUES(:username, :password, :email, :name)";

        if($stmt = $this->_db->prepare($sql)) {
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":name", $name, PDO::PARAM_STR);
            if (!$stmt->execute()) {
                // if failed
                return [
                    'success' => false,
                    'messages' => [
                        'Unable to create new account...',
                    ],
                ];
            }

            $stmt->closeCursor();
            return [
                'success' => true,
                'id' => $this->_db->lastInsertId(),
                'messages' => [
                    'New account was created successfully!',
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unspecified error...',
            ],
        ];
    }
}
?>