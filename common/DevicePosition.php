<?php
class DevicePosition {
    private $_db;

    private $isAdmin;

    public function __construct($db = null, $isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
        if (is_object($db))
        {
            $this->_db = $db;
        }
        else
        {
            $dsn = "mysql:host=". DB_HOST .";dbname=". DB_NAME;
            $this->_db = new PDO($dsn, DB_USER, DB_PASS);
        }
    }

    public function getLatestPositions() {
        if (!$this->isAdmin && (empty($_SESSION['loggedIn']) || empty($_SESSION['username']) || empty($_SESSION['userId'])))
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];

        $sql = "SELECT * FROM device_position WHERE id IN (SELECT MAX(id) FROM device_position GROUP BY device_id)";
        try
        {
            $stmt = $this->_db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch();
            $stmt->closeCursor();

            return [
                'success' => true,
                'devices' => $result,
                'messages' => [
                ],
            ];
        }
        catch(PDOException $e)
        {
            return [
                'success' => false,
                'messages' => [
                    $e->getMessage(),
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unknown error...'
            ],
        ];
    }

    public function getDevicesList() {
        if (!$this->isAdmin && (empty($_SESSION['loggedIn']) || empty($_SESSION['username']) || empty($_SESSION['userId'])))
            return [
                'success' => false,
                'messages' => [
                    'Unauthorized!..',
                ],
            ];

        $sql = "SELECT DISTINCT device_id
            FROM device_position";
        try
        {
            $stmt = $this->_db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch();
            $stmt->closeCursor();

            return [
                'success' => true,
                'devices' => $result,
                'messages' => [
                ],
            ];
        }
        catch(PDOException $e)
        {
            return [
                'success' => false,
                'messages' => [
                    $e->getMessage(),
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unknown error...'
            ],
        ];
    }

    public function create($deviceId, $deviceName, $lat, $long, $raw) {

        // Trying to create new row
        $sql = "INSERT INTO device_position(device_id, device_name, longitude, latitude, params)
                VALUES(:deviceId, :deviceName, :long, :lat, :params)";

        if($stmt = $this->_db->prepare($sql)) {
            $params = json_encode($raw);
            $stmt->bindParam(":deviceId", $deviceId, PDO::PARAM_STR);
            $stmt->bindParam(":deviceName", $deviceName, PDO::PARAM_STR);
            $stmt->bindParam(":long", strval($long), PDO::PARAM_STR);
            $stmt->bindParam(":lat", strval($lat), PDO::PARAM_STR);
            $stmt->bindParam(":params", $params, PDO::PARAM_STR);

            if (!$stmt->execute()) {
                // if failed
                return [
                    'success' => false,
                    'messages' => [
                        'Unable to create new row...',
                    ],
                ];
            }

            $stmt->closeCursor();
            return [
                'success' => true,
                'id' => $this->_db->lastInsertId(),
                'messages' => [
                    'New row was created successfully!',
                ],
            ];
        }

        return [
            'success' => false,
            'messages' => [
                'Unspecified error...',
            ],
        ];
    }
}
?>